  'use strict'

  class Animal {
    constructor (name) {
      this.name = name
    }

    static speak () {
      return `${this.name}, say hello`
    }
  }

  class Dog extends Animal {
    constructor (name) {
      super(name)
    }

    static sua () {
    console.log(this.speak())
    return `I'm a dog`
  }
}

let dog = new Dog('Cong')
console.log(Dog.sua())
