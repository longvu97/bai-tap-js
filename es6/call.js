let person1 = {
  name: 'cong mat choa',

  sayHello: function (param) {
    return `${param}, ${this.name}`
  }
}

let person = {
  name: 'vu'
}

console.log(person1.sayHello.call(person, 'hello'))
