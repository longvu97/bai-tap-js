'use strict'

const dupStr = str => {
  let strResult = ''

  for (let value of str) {
    if (strResult.indexOf(value) === -1) {
      strResult += value
    }
  }
  return strResult
}

const STR = 'lonngvuuu'

console.log(dupStr(STR))
