(() => {
  const X1 = 1
  const X2 = 2

  const sum = (x1, x2) => {
    return x1 + x2
  }

  console.log(sum(X1, X2))
})()
