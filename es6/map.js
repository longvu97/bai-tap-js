const union = (mapA, mapB) => {
  let _union = new Map(mapA)
  for (const [key, elem] of mapB) {
    _union.set(key, elem)
  }

  return _union
}
arrA = [
  ['1', 1],
  ['2', 2],
  ['3', 3],
  ['4', 4]
]
arrB = [
  ['a', 'a'],
  ['b', 'b'],
  ['c', 'c'],
  ['d', 'a']
]
mapA = new Map(arrA)
mapB = new Map(arrB)

console.log(mapA)
console.log(mapB)

console.log(union(mapA, mapB))
