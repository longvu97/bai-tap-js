let gem = {
  color: 'red',
  weight: 200,

  getGem () {
    return `${this.color} and ${this.weight}g`
  }
}

let person = function (person, age) {
  return `${this.getGem()} of ${person}, ${age} years old`
}
console.log(person.apply(gem, ['Vu', 21]))
