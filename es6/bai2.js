'use strict'

const obj = {
    'a': 1,
    'b': 2,
    'c': 3
}

((obj, index = 'b') => {
    for (const key in obj) {
        if(index === key)
            delete obj.index
    }

    console.log(obj)
})()