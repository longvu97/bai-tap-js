let person1 = {
  name: 'cong mat choa',

  sayHello: function (param) {
    return `${param}, ${this.name}`
  }
}

let person = {
  name: 'vu'
}

let sayNew = person1.sayHello.bind(person, 'hello')
console.log(sayNew())
