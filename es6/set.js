'use strict'

const union = (setA, setB) => {
  let _union = new Set(setA)
  for (const elem of setB) {
    _union.add(elem)
  }

  return _union
}

let strA = 'LongVuuuuuuuu'
let strB = 'LeVan'

let setA = new Set(strA)
let setB = new Set(strB)

let merge = union(setA, setB)

console.log([...setA].join(''))
