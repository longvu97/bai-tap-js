var forOf = (str) => {
    var strTemp = '';

    for (const value of str) {
        if (strTemp.indexOf(value) === -1){
            strTemp += value;
        }
    }
    return strTemp;
}

var str = "leeeevannnlonnngggvuuuuuu";

console.log(forOf(str));
