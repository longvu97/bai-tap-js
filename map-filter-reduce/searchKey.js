const searchKey = (arr, key) => {
  return arr.filter(el => el.toLowerCase().indexOf(key.toLowerCase()) > -1)
}

const ARR = ['long', 'vu', 'le', 'van']

console.log(searchKey(ARR, 'n'))
