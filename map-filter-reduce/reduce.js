let arr = ['longvu', 'levan']

let total = arr.reduce((acc, cur) => {
  return acc + cur
}, 5)

let integrated = arr.reduce((acc, cur) => {
  return acc * cur
}, 5)

let total1 = arr.reduce((acc, cur, index) => {
  return acc + cur + index
})

let str = arr.reduce(x => x === 'longvu' ? x : 'khong co')

console.log(str)
