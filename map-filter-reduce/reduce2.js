let people = [
  { name: 'Alice', age: 21 },
  { name: 'Max', age: 20 },
  { name: 'Jane', age: 20 },
  { name: 'Jane', age: 21 }
]

const groupBy = (objArr, param) => {
  return objArr.reduce((acc, obj) => {
    let key = obj[param]
    if (!acc[key]) {
      acc[key] = []
    }
    acc[key].push(obj)
    return acc
  }, {})
}

console.log(groupBy(people, 'age'))
