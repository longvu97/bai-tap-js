const OBJ = [{
  id: 1
}, {
  id: 2
}, {
  id: 'hello'
}, {
  id: 'world'
}]

const isNumber = (obj) => {
  return obj !== undefined && obj !== isNaN() && typeof obj === 'number'
}

const filterById = (obj) => {
  if (isNumber(obj.id)) { return true }
  return false
}

const FILTER = OBJ.filter(filterById)

console.log(FILTER)
