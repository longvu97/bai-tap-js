function numStringRange(start, end, step) {
    var arr = [];
    if (typeof start !== typeof end || start > end) {
        console.log("Nhap lai");
        return;
    } else {

        if (typeof start == 'string' && typeof end == 'string') {
            var code1 = start.charCodeAt();
            var code2 = end.charCodeAt();
            for (var i = code1; i <= code2; i += step) {
                arr.push(String.fromCharCode(i));
            };

        } else {
            for (var i = start; i <= end; i += step) {
                arr.push(i);
            }
        }
        return arr.join(' ');
    }
}


console.log(numStringRange("a", "b", 2));