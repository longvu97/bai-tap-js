(() => {
  const reverseStr = (str) => {
    let str1 = str.split('').reverse().join('')
    return str1
  }

  console.log(reverseStr('123'))
})()
