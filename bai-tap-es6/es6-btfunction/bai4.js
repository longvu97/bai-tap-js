(() => {
  const diffi = (str) => {
    let len = Math.pow(2, str.length)
    let arr = str.split('')

    for (let i = 1; i < len - 1; i++) {
      let result = ''
      for (const [j, value] of arr.entries()) {
        if (i & Math.pow(2, j)) { result += value }
      }
      console.log(result)
    }
  }

  diffi('quoc')
})()
