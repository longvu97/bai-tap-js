(() => {
  let n = 10
  let sum = 0
  let i = 0
  while (i < n) {
    if (i % 2 === 0) { sum += i }
    i++
  }

  console.log(sum)
})()
