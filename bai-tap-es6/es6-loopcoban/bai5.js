() => {
  const socs = x => {
    let count = 0
    while (x > 1) {
      x = parseInt(x) / 10
      count++
    }
    return count
  }

  const test = number => {
    let temp = number
    let mu = socs(number)
    let sum = 0
    while (parseInt(temp) != 0) {
      let r = parseInt(temp % 10)
      sum += Math.pow(r, mu)
      temp /= 10
    }
    if (sum === number) {
      return true
    } else {
      return false
    }
  }

  for (let i = 0; i < 154; i++) {
    if (test(i)) {
      console.log(i)
    }
  }
}
