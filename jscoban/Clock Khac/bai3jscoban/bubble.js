function Bulle(x, y) {
  this.x = x;
  this.y = y;
  this.diameter = 50;
  this.speed = 3;
  this.colorRed = random(0, 255);
  this.colorGreen = random(0, 255);
  this.colorBlue = random(0, 255);
  this.display = function () {
    return ellipse(this.x, this.y, this.diameter, this.diameter)
  }
}

var popy = [];
var canvas;

function setup() {
  canvas = createCanvas(1920, 650);
  canvas.position(0, 0);
  canvas.style('z-index', '-1');
}

function draw() {
  background(0);
  for (let i = 0; i < popy.length; i++) {
    fill(popy[i].colorRed, popy[i].colorGreen, popy[i].colorBlue);

    popy[i].display();

    if (popy[i].y > height) {
      popy[i].speed = -3;
    }
    if (popy[i].y <= 0) {
      popy[i].speed = 3;
    }
    popy[i].y += popy[i].speed;
  }

  // here u define how many bubbles u want to keep on screen..
  if (popy.length > 30) {
    popy.splice(0, 1);
  }

}

function mousePressed() {
  popy.push(new Bulle(mouseX, mouseY));
}