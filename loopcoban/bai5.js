function socs(x) {
    var count = 0;
    while (x > 1) {
        x = parseInt(x) / 10;
        count++;
    }return count;
}

function test(number) {
    var temp = number;
    var mu = socs(number);
    var sum = 0;
    while (parseInt(temp) != 0) {
        var r = parseInt(temp % 10);
        sum += Math.pow(r, mu);
        temp /= 10;
    }
    if (sum === number)
        return true;
    else
        return false;
}

for (var i = 0; i < 154; i++) {
    if (test(i))
        console.log(i);
}