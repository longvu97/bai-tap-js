function unionElements (arr1, arr2) {
  var result = []
  arr = arr1.concat(arr2)

  for (var i = 0; i < arr1.length; i++) {
    if (arr2.indexOf(arr1[i]) === -1) {
      result.push(arr1[i])
    }
  }

  for (var i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) === -1) {
      result.push(arr2[i])
    }
  }

  return result
}
var arr1 = [1, 2, 3]
var arr2 = [100, 2, 1, 10]
console.log(unionElements(arr1, arr2))
