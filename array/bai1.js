var arr = [24, 50, 3, 10]

function sortArr (arr) {
  var temp
  for (var i = 0; i < arr.length; i++) {
    for (var j = i; j < arr.length; j++) {
      if (arr[i] > arr[j]) {
        temp = arr[i]
        arr[i] = arr[j]
        arr[j] = temp
      }
    }
  }
  return arr
}

'abc'
'cdf'
'addddeee'
'adddeeeeee'
console.log(sortArr(arr))
