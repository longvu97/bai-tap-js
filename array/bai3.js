function check (year) {
  var str = year.toString()
  for (var i = 0; i < str.length; i++) {
    if (str[i] < 48 && str[i] > 57) {
      return false
    }
  }
  return true
}

function years (startYear, endYear) {
  var arr = []

  if (startYear > endYear || startYear < 0 || endYear < 0 || !check(startYear) || !check(endYear)) {
    console.log('Nhap lai')
  } else {
    for (var i = startYear; i <= endYear; i++) {
      if (i % 4 === 0 && i % 100 !== 0) {
        console.log('Nam Nhuan: ' + i)
      }
    }
  }
}

years(2000, 2012)
